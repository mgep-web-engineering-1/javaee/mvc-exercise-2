# MVC Exercise 2

This time, we mixed [MVC-Exercise-1](https://gitlab.com/mgep-web-engineering-1/javaee/mvc-exercise-1) and [12-Friendly-URLs](https://gitlab.com/mgep-web-engineering-1/javaee/12-friendly-urls). That way, we have a User CRUD with clean or friendly URLs.

(Favicon has also been added to header.jsp)

We have also added custome error pages.

## Some Explainations

In order to manage error pages and have customized error pages, we can dispatch to an error view adding the error code:

```java
session.setAttribute("error", "error.404.not_found");
session.setAttribute("errorCode", errorCode);
RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/error.jsp");
response.setStatus(errorCode);
dispatcher.forward(request, response);
```

Then, ```error.jsp``` will show the error:

```jsp

<c:set var="pageTitle" scope="request" value="error.${sessionScope.errorCode}.title"/>
...
<div class="card">
    <h2 class="card-title"><fmt:message key="error.${sessionScope.errorCode}.title"/></h2>
    <div class="card-body">
        <p><fmt:message key="error.${sessionScope.errorCode}.message"/></p>
    </div>
</div>
```

### User controller

Now we will use ControllerHelper in UserController:

```java
// UserController.java
...
protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    session = request.getSession(true);
    ControllerHelper helper = new ControllerHelper(request);
    String action = helper.getAction();

    switch (action) {
        case "delete" ->            deleteUser(request, response, helper.getId());
        case "create", "edit" ->    showUserForm(request, response, helper.getId());
        case "view" ->              showUser(request, response, helper.getId());
        default ->                  listUsers(request, response);
    }
}
...
protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    session = request.getSession(true);
    ControllerHelper helper = new ControllerHelper(request);
    String action = helper.getAction();

    switch (action) {
        case "create" ->    createUser(request, response);
        case "edit" ->      editUser(request, response, helper.getId());
        default ->          listUsers(request, response);
    }
}
...
```

### Permissions

User filter will listen to ```/user/*```

```java
// UserFilter.java
@WebFilter("/user/*")
```

It will will get the action using ```ControllerHelper```:

```java
// UserFilter.java
ControllerHelper helper = new ControllerHelper(request);
String action = helper.getAction();
```

For edit&delete, ```/user/{id}/{action}``` id and session userId should be the same.

```java
...
case "edit":
case "delete":
    code = filterModification(helper, session);
    break;
...
private int filterModification(ControllerHelper helper, HttpSession session) {
    User user = (User) session.getAttribute("user");

    if (user == null || user.getUserId() != helper.getId()){
        // Guard clause
        System.out.println("User trying to modify other user.");
        session.setAttribute("error", "error.403.not_own_user");
        return HttpServletResponse.SC_FORBIDDEN;
    }

    System.out.println("User modifies it's own user.");
    return HttpServletResponse.SC_OK;
}
```

Anyone can create a user.

```java
// UserFilter.java
...
case "create":
    System.out.println("User creation allowed for anybody.");
    code = HttpServletResponse.SC_OK;
    break;
...
```

For list&view, a user must be on session.

```java
// UserFilter.java
...
case "view":
case "list":
    code = filterShow(session);
    break;
...
private int filterShow(HttpSession session) {
    User user = (User) session.getAttribute("user");

    if (user == null){
        // Guard clause
        System.out.println("View/List actions need the user to be in session.");
        session.setAttribute("error", "error.403.not_session_user");
        return HttpServletResponse.SC_FORBIDDEN;
    }
    
    System.out.println("User in session. They can view/list user(s).");
    return HttpServletResponse.SC_OK;
}
```

## MVC Exercise 3

Based on this exercise, create a new content type: **NewsItem** (in english, *New* is not the singular of *News*).

* There is a table in the database for this content type: news_item (Title | Body | Author | Date | Language).
* Create a CRUD for that content type:
  * News Item JavaBean.
  * News Item DAO & Facade.
  * News Item views (list, view, form...)
  * Friendly URL controller.
  * Filter Permissions:
    * Anyone can see the News Item List and Single News Items.
    * Only logged users can create news items (you have to get the author id and language, date logic is already in the database design).
    * Only authors can edit/delete their News Items.
