package edu.mondragon.webeng1.mvc_exercise.controller;

import java.io.IOException;
import java.util.Optional;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import edu.mondragon.webeng1.mvc_exercise.domain.user.model.User;
import edu.mondragon.webeng1.mvc_exercise.domain.user.dao.UserFacade;
import edu.mondragon.webeng1.mvc_exercise.helper.ControllerHelper;

@WebServlet(name = "LoginController", urlPatterns = { "/login/*" })
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        ControllerHelper helper = new ControllerHelper(request);
        String action = helper.getAction();
        HttpSession session = request.getSession(true);

        switch (action) {
        case "login" -> login(session, request, response);
        default -> logout(session, request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    private void login(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = Optional.ofNullable(request.getParameter("username")).orElse("");
        String password = Optional.ofNullable(request.getParameter("password")).orElse("");
        System.out.println("Username: " + username);
        System.out.println("Password: " + password
            + " (This should never be done in real projects! Printing passwords in logs is a bad practice.)");
        User user = null;

        // Check if the user exists in the database/properties file
        UserFacade uf = new UserFacade();
        user = uf.loadUser(username, password);


        // Save login result in session
        if (user != null) {
            session.setAttribute("user", user);
            session.setAttribute("message", "message.login");
        } else {
            session.removeAttribute("user");
            session.setAttribute("username", username);
            session.setAttribute("error", "error.login");
        }
        response.sendRedirect("/");
    }

    private void logout(HttpSession session, HttpServletRequest request, HttpServletResponse response)
        throws IOException {
        session.removeAttribute("user");
        session.setAttribute("message", "message.logout");

        response.sendRedirect("/");
    }

}