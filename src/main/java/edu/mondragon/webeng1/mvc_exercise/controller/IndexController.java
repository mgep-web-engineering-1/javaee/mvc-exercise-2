package edu.mondragon.webeng1.mvc_exercise.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This class will show the main page (as home.jsp is not directly accessible).
 * Be careful, becouse an index.jsp or index.html file will make this controller
 * unaccessible.
 * 
 * @author aperez
 *
 */
@WebServlet(name = "IndexController", urlPatterns = { "/index.html" })
public class IndexController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public IndexController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        System.out.println("Index Controller");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/home.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
