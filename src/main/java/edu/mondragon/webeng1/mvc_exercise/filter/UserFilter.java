package edu.mondragon.webeng1.mvc_exercise.filter;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import edu.mondragon.webeng1.mvc_exercise.domain.user.model.User;
import edu.mondragon.webeng1.mvc_exercise.helper.ControllerHelper;

/**
 * Servlet Filter implementation class UserFilter
 */
@WebFilter("/user/*")
public class UserFilter implements Filter {

    public UserFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        System.out.println("User Filter");
        HttpServletRequest request = (HttpServletRequest) req;
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(true);

        ControllerHelper helper = new ControllerHelper(request);
        String action = helper.getAction();

        int code;

        switch (action) {
            case "create":
                System.out.println("User creation allowed for anybody.");
                code = HttpServletResponse.SC_OK;
                break;
            case "edit":
            case "delete":
                code = filterModification(helper, session);
                break;
            case "view":
            case "list":
                code = filterShow(session);
                break;
            default:
                System.out.println("Unknown User action: " + action);
                code = HttpServletResponse.SC_BAD_REQUEST;
                session.setAttribute("error", "error.400.unknown_action");
        }

        if (code == HttpServletResponse.SC_OK) {
            chain.doFilter(req, res);
        } else {
            session.setAttribute("errorCode", code);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/error.jsp");
            response.setStatus(code);
            dispatcher.forward(request, response);
        }
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

    private int filterModification(ControllerHelper helper, HttpSession session) {
        User user = (User) session.getAttribute("user");

        if (user == null || user.getUserId() != helper.getId()){
            // Guard clause
            System.out.println("User trying to modify other user.");
            session.setAttribute("error", "error.403.not_own_user");
            return HttpServletResponse.SC_FORBIDDEN;
        }

        System.out.println("User modifies it's own user.");
        return HttpServletResponse.SC_OK;
    }

    private int filterShow(HttpSession session) {
        User user = (User) session.getAttribute("user");

        if (user == null){
            // Guard clause
            System.out.println("View/List actions need the user to be in session.");
            session.setAttribute("error", "error.403.not_session_user");
            return HttpServletResponse.SC_FORBIDDEN;
        }
        
        System.out.println("User in session. They can view/list user(s).");
        return HttpServletResponse.SC_OK;
    }
}
